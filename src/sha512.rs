/**
 * @file sha512.h
 * @brief SHA-512 (Secure Hash Algorithm 512)
 *
 * @section License
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * Copyright (C) 2010-2021 Oryx Embedded SARL. All rights reserved.
 *
 * This file is part of CycloneCRYPTO Open.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @author Oryx Embedded SARL (www.oryx-embedded.com)
 * @version 2.0.2
 *
 * Ported from C to Rust 2020 by microengineer18@gmail.com
 **/

use digest::consts::{U128, U64};
use digest::{BlockInput, FixedOutputDirty, Output, Reset, Update};

type BlockSize = U128;

const K: [u64; 80] = [
    0x428A2F98D728AE22,
    0x7137449123EF65CD,
    0xB5C0FBCFEC4D3B2F,
    0xE9B5DBA58189DBBC,
    0x3956C25BF348B538,
    0x59F111F1B605D019,
    0x923F82A4AF194F9B,
    0xAB1C5ED5DA6D8118,
    0xD807AA98A3030242,
    0x12835B0145706FBE,
    0x243185BE4EE4B28C,
    0x550C7DC3D5FFB4E2,
    0x72BE5D74F27B896F,
    0x80DEB1FE3B1696B1,
    0x9BDC06A725C71235,
    0xC19BF174CF692694,
    0xE49B69C19EF14AD2,
    0xEFBE4786384F25E3,
    0x0FC19DC68B8CD5B5,
    0x240CA1CC77AC9C65,
    0x2DE92C6F592B0275,
    0x4A7484AA6EA6E483,
    0x5CB0A9DCBD41FBD4,
    0x76F988DA831153B5,
    0x983E5152EE66DFAB,
    0xA831C66D2DB43210,
    0xB00327C898FB213F,
    0xBF597FC7BEEF0EE4,
    0xC6E00BF33DA88FC2,
    0xD5A79147930AA725,
    0x06CA6351E003826F,
    0x142929670A0E6E70,
    0x27B70A8546D22FFC,
    0x2E1B21385C26C926,
    0x4D2C6DFC5AC42AED,
    0x53380D139D95B3DF,
    0x650A73548BAF63DE,
    0x766A0ABB3C77B2A8,
    0x81C2C92E47EDAEE6,
    0x92722C851482353B,
    0xA2BFE8A14CF10364,
    0xA81A664BBC423001,
    0xC24B8B70D0F89791,
    0xC76C51A30654BE30,
    0xD192E819D6EF5218,
    0xD69906245565A910,
    0xF40E35855771202A,
    0x106AA07032BBD1B8,
    0x19A4C116B8D2D0C8,
    0x1E376C085141AB53,
    0x2748774CDF8EEB99,
    0x34B0BCB5E19B48A8,
    0x391C0CB3C5C95A63,
    0x4ED8AA4AE3418ACB,
    0x5B9CCA4F7763E373,
    0x682E6FF3D6B2B8A3,
    0x748F82EE5DEFB2FC,
    0x78A5636F43172F60,
    0x84C87814A1F0AB72,
    0x8CC702081A6439EC,
    0x90BEFFFA23631E28,
    0xA4506CEBDE82BDE9,
    0xBEF9A3F7B2C67915,
    0xC67178F2E372532B,
    0xCA273ECEEA26619C,
    0xD186B8C721C0C207,
    0xEADA7DD6CDE0EB1E,
    0xF57D4F7FEE6ED178,
    0x06F067AA72176FBA,
    0x0A637DC5A2C898A6,
    0x113F9804BEF90DAE,
    0x1B710B35131C471B,
    0x28DB77F523047D84,
    0x32CAAB7B40C72493,
    0x3C9EBE0A15C9BEBC,
    0x431D67C49C100D4C,
    0x4CC5D4BECB3E42B6,
    0x597F299CFC657E2A,
    0x5FCB6FAB3AD6FAEC,
    0x6C44198C4A475817,
];

const PADDING: [u8; 128] = [
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
];


#[derive(Clone)]
pub struct SHA512 {
    h: [u64; 8],
    buffer: [u8; 128],
    size: usize,
    total_size: u64,
}

macro_rules! CH {
    ($x:expr, $y:expr, $z:expr) => {
        ((($x) & ($y)) | (!($x) & ($z)))
    };
}

macro_rules! MAJ {
    ($x:expr, $y:expr, $z:expr) => {
        ((($x) & ($y)) | (($x) & ($z)) | (($y) & ($z)))
    };
}

macro_rules! ROR64 {
    ($a:expr,$n:expr) => {
        ((($a) >> ($n)) | (($a) << (64 - ($n))))
    };
}

macro_rules! SHR64 {
    ($a:expr,$n:expr) => {
        (($a) >> ($n))
    };
}

macro_rules! SIGMA1 {
    ($x:expr) => {
        (ROR64!($x, 28) ^ ROR64!($x, 34) ^ ROR64!($x, 39))
    };
}

macro_rules! SIGMA2 {
    ($x:expr) => {
        (ROR64!($x, 14) ^ ROR64!($x, 18) ^ ROR64!($x, 41))
    };
}

macro_rules! SIGMA3 {
    ($x:expr) => {
        (ROR64!($x, 1) ^ ROR64!($x, 8) ^ SHR64!($x, 7))
    };
}

macro_rules! SIGMA4 {
    ($x:expr) => {
        (ROR64!($x, 19) ^ ROR64!($x, 61) ^ SHR64!($x, 6))
    };
}

impl SHA512 {
    pub fn new() -> Self {
        let mut h = Self {
            h: [0u64; 8],
            buffer: [0u8; 128],
            size: 0,
            total_size: 0,
        };
        h.init();
        h
    }

    pub fn compute(&mut self, data: &[u8], digest: &mut [u8; 64]) -> bool {
        self.init();
        self.update(data);
        self.digest(digest);
        true
    }

    pub fn init(&mut self) {
        //Set initial hash value
        self.h[0] = 0x6A09E667F3BCC908;
        self.h[1] = 0xBB67AE8584CAA73B;
        self.h[2] = 0x3C6EF372FE94F82B;
        self.h[3] = 0xA54FF53A5F1D36F1;
        self.h[4] = 0x510E527FADE682D1;
        self.h[5] = 0x9B05688C2B3E6C1F;
        self.h[6] = 0x1F83D9ABFB41BD6B;
        self.h[7] = 0x5BE0CD19137E2179;

        //Number of bytes in the buffer
        self.size = 0;
        //Total length of the message
        self.total_size = 0;
    }

    pub fn update(&mut self, data: &[u8]) {
        let mut length = data.len();
        let mut data_ofs = 0;
        while length > 0 {
            let n = if length < 128 - self.size {
                length
            } else {
                128 - self.size
            };
            self.buffer[self.size..self.size + n].copy_from_slice(&data[data_ofs..data_ofs + n]);
            self.size += n;
            self.total_size += n as u64;
            length -= n;
            data_ofs += n;
            if self.size == 128 {
                self.process_block();
                self.size = 0;
            }
        }
    }

    pub fn digest(&mut self, digest: &mut [u8; 64]) {
        let total_size = self.total_size * 8;
        let padding_size = if self.size < 112 {
            112 - self.size
        } else {
            128 + 112 - self.size
        };

        self.update(&PADDING[0..padding_size]);
        self.buffer[14 * 8..(14 + 1) * 8].copy_from_slice(&0u64.to_be_bytes()[..]);
        self.buffer[15 * 8..(15 + 1) * 8].copy_from_slice(&total_size.to_be_bytes()[..]);
        self.process_block();

        for i in 0..8 {
            digest[i * 8..(i + 1) * 8].copy_from_slice(&self.h[i].to_be_bytes()[..]);
        }
    }

    fn process_block(&mut self) {
        let mut a: u64 = self.h[0];
        let mut b: u64 = self.h[1];
        let mut c: u64 = self.h[2];
        let mut d: u64 = self.h[3];
        let mut e: u64 = self.h[4];
        let mut f: u64 = self.h[5];
        let mut g: u64 = self.h[6];
        let mut h: u64 = self.h[7];

        let mut w = [0u64; 16];
        for i in 0..16 {
            let mut bytes = [0u8; 8];
            bytes.copy_from_slice(&self.buffer[i * 8..(i + 1) * 8]);
            w[i] = u64::from_be_bytes(bytes);
        }

        for t in 0..80 {
            if t >= 16 {
                w[t & 0xf] =
                    w[t & 0xf]
                        .wrapping_add(SIGMA4!(w[(t + 14) & 0xf]).wrapping_add(
                            w[(t + 9) & 0xf].wrapping_add(SIGMA3!(w[(t + 1) & 0xf])),
                        ));
            }

            let temp1 = h.wrapping_add(
                SIGMA2!(e).wrapping_add(CH!(e, f, g).wrapping_add(K[t].wrapping_add(w[t & 0xf]))),
            );

            let temp2 = SIGMA1!(a).wrapping_add(MAJ!(a, b, c));

            h = g;
            g = f;
            f = e;
            e = d.wrapping_add(temp1);
            d = c;
            c = b;
            b = a;
            a = temp1.wrapping_add(temp2);
        }

        self.h[0] = self.h[0].wrapping_add(a);
        self.h[1] = self.h[1].wrapping_add(b);
        self.h[2] = self.h[2].wrapping_add(c);
        self.h[3] = self.h[3].wrapping_add(d);
        self.h[4] = self.h[4].wrapping_add(e);
        self.h[5] = self.h[5].wrapping_add(f);
        self.h[6] = self.h[6].wrapping_add(g);
        self.h[7] = self.h[7].wrapping_add(h);
    }
}

impl Default for SHA512 {
    fn default() -> Self {
        SHA512::new()
    }
}

impl BlockInput for SHA512 {
    type BlockSize = BlockSize;
}

impl Update for SHA512 {
    fn update(&mut self, input: impl AsRef<[u8]>) {
        self.update(input.as_ref());
    }
}

impl FixedOutputDirty for SHA512 {
    type OutputSize = U64;
    fn finalize_into_dirty(&mut self, out: &mut Output<Self>) {
        let mut digest = [0u8;64];
        self.digest(&mut digest);
        out.copy_from_slice(&digest);
    }
}

impl Reset for SHA512 {
    fn reset(&mut self) {
        self.init();
    }
}

