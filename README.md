# rust-sha512

(Almost) dependency-free small sha512 implementation. Much smaller (but also slower) than sha2::sha512.

Implements the Digest traits.
